# NB
1. This code has no warranty et al. Don't use it for important stuff.

2. The code is free to use for anything that is honourable, just, and pure. Acknowledgements can be made to gitlab.com/lordratte

3. Don't run TishlyWiki files modified by other people.

# What is TishlyWiki?
TishlyWiki is a single-file, http server that stores its servable content after the server logic. Whenever it gets a request, it reads itself to get the content section (e.g. html) and serves that.

If it recieves a PUT request, it replaces the old content section with with content of the put request. This is designed to work especially well with [TiddlyWiki](https://tiddlywiki.com/), the single-file Wikipedia/notebook, which often relies on external plugins.

**tl;dr**

Executable html files

# Quickstart
```bash
curl https://gitlab.com/lordratte/tishlywiki/-/jobs/artifacts/master/raw/out/build.html?job=build > build.html
chmod +x build.html
./build.html -s -e handler
```
For more information see the links for "Download and Install" and "Running" bellow.

For Windows usage see the links for "Windows" and "Running".

# Docs

* [Acknowledgements](./docs/Acknowledgements.md)
* [Using on Windows](./docs/Windows.md)
* [Download and Install](./docs/Download_and_Install.md)
* [Building](./docs/Building.md)
* [Running](./docs/Running.md)

# Keep in Mind

1. It can be still be opened and edited via some other saving methods (TiddlyDrive has been most tested).

2. It relies heavily on the lenient syntax of modern browsers to a. be a valid bash/batch script and b. render properly as html. Needless to say, it won't work on all browsers. The hackiest part of the thing is possibly a bit of JavaScript that hides the first few characters of the file, which unlike the rest of the script, can't be hidden with an html comment.

3. It's still under heavy development.

## Reverting

If you want to convert your TishlyWiki back to a TiddlyWiki file, open it as a normal html file, delete `$:/core/save/all` to reset it to its Shadow, and then save the file using the download-saver.

Alternatively, refer to [Command Line Method](./docs/Download_and_Install.md/#method-2-command-line-method) under the "Download and Install" docs.
