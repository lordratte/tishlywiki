PAYLOAD="$(cat "$2/dynamic/server.ps1" | awk -v escape='>,<,&,),\"' 'BEGIN{split(escape, aesc, ",")} {printf("echo(") ; gsub(/\^)/, "^^"); gsub("\\|", "^|"); gsub("%", "%%"); for (i=1; i <= length(aesc); i++) { gsub(aesc[i], "^" aesc[i])} ; print}' )"
NEW="$(cat "$1" | awk -v var="$PAYLOAD" '/^__PowerShellPlaceholder__$/ { print var; next } 1' )"

printf '%s\n' "$NEW" > "$1"

