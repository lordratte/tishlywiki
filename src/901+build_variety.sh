# Auomatic plugin creation

json_escape () {
    cat "$1" | python -c 'import json,sys; print(json.dumps(sys.stdin.read()))'
}

out=${1%\/*}

# format: $(everything before the placeholder)$(jsonified code)$(everything after the placeholder)
CODE="$(json_escape "$1")";
POST="$(awk '{if(a) print; if(match($0, "__plugintext__")){ a=1; sub("^.*__plugintext__\"", ""); print}; }' "$2/installers/tishlywiki.json")";
printf '%s' "$PRE$CODE$POST" >| "$out/tishlywiki.json";

# Standalone tiddler creation (for manual application)
cp "$2/installers/temp_tishlywiki_header.txt" "$out/temp_tishlywiki.tid"
cat "$1" >> "$out/temp_tishlywiki.tid"
