foreach ($arg in $args) {
    if (!$file) {$file=$arg; continue;}
    if (($arg.ToCharArray()) -contains "=") {
        $split = $arg.Split("=");
        $key=$split[0];
        $val=$split[1];
        if ($key -eq "port") {
            $o_port = $val;
        } elseif ($key -eq "host") {
            $o_host = $val;
        }
    } else {
        write-host $arg "should have been in the format KEY=VALUE"
        EXIT
    }
}

if ($null -eq $o_host) {
    $o_host="127.0.0.1";
}
if ($null -eq $o_port) {
    $o_port="8080";
}


# Setup Server
$http = [System.Net.HttpListener]::new() 
$http.Prefixes.Add("http://$($o_host):$($o_port)/")
$http.Start()

if ($http.IsListening) {
    write-host "TishlyWiki server started on $($http.Prefixes)" -f 'black' -b 'gre'
}

function prepare {
    param (
        $Response
    )
    $Response.AddHeader('Server', $file);
}

#Listening Loop
while ($http.IsListening) {
    # Request handler
    $context = $http.GetContext()
    write-host "$($context.Request.Url) $($context.Request.HttpMethod)=> $($context.Request.UserHostAddress)" -f 'mag'

    # GET /
    if ($context.Request.HttpMethod -eq 'GET' -and $context.Request.RawUrl -eq '/') {

        $source = Get-Content -Encoding UTF8 $file;

        $start = 0;
        foreach ($line in $source) {
            $start++;
            if ($line -eq '<!--DATA-'+'->') {
                break;
            }
        }

        [string]$html = $source[$start..($source.Length-1)] -join "`n"
        prepare($context.Response);
        #resposed to the request
        $buffer = [System.Text.Encoding]::UTF8.GetBytes($html) # convert htmtl to bytes
        $context.Response.ContentLength64 = $buffer.Length
        $context.Response.OutputStream.Write($buffer, 0, $buffer.Length) #stream to broswer
        $context.Response.OutputStream.Close() # close the response
    }

    # OPTIONS /
    if ($context.Request.HttpMethod -eq 'OPTIONS' -and $context.Request.RawUrl -eq '/') {
        prepare($context.Response);
        $context.Response.AddHeader('dav', 'true');
        $context.Response.OutputStream.Close() # close the response
    }

    # PUT /
    if ($context.Request.HttpMethod -eq 'PUT' -and $context.Request.RawUrl -eq '/') {
        if($context.Request.HasEntityBody) {
            prepare($context.Response);

            $reader = [System.IO.StreamReader]::new($context.Request.InputStream, $context.Request.ContentEncoding);
            $content = $reader.ReadToEnd();

            $source = Get-Content $file;

            $end = 0;
            foreach ($line in $source) {
                $end++;
                if ($line -eq '<!--DATA-'+'->') {
                    break;
                }
            }
            [string]$html = $source[0..$end] -join "`n"
            $html += "`n" + $content;
            $html | Out-File -FilePath $file -Encoding ASCII;
            $context.Response.OutputStream.Close() # close the response
        }
    }
    

}
