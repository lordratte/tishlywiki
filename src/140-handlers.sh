# Request-Line HTTP RFC 2616 $5.1
read -r line || fail_with 400

# strip trailing CR if it exists
line=${line%%$'\r'}
recv "$line"

read -r REQUEST_METHOD REQUEST_URI REQUEST_HTTP_VERSION <<<"$line"

[[ -n $REQUEST_METHOD &&
   -n $REQUEST_URI &&
   -n $REQUEST_HTTP_VERSION ]] || fail_with 400

case "$REQUEST_METHOD" in
  "GET")
    declare -a REQUEST_HEADERS

    while read -r line; do
      line=${line%%$'\r'}
      recv "$line"

      # If we've reached the end of the headers, break.
      [[ -z $line ]] && break

      REQUEST_HEADERS+=("$line")
    done;

    if [[ "$REQUEST_URI" =~ ^/$ ]]; then
      file=$(get_payload DATA)
      serve_file "$file"
    fi
  ;;
  "OPTIONS")
    while read -r line; do
      line=${line%%$'\r'}
      recv "$line"
      [[ -z $line ]] && break
    done;
    add_response_header "dav" "true";
    fail_with 200;
  ;;
  "PUT")
    declare -a REQUEST_HEADERS

    NEWFILE=$(mktemp --suffix="_$(basename "$0")");
    get_payload . DATA > $NEWFILE
    echo "<!""--DATA--"">" >> $NEWFILE
    read -r CONTENT_LENGTH_OFFSET < <(stat -c'%s' "$NEWFILE")

    eof=
    while [[ -z $eof ]]; do
      read -r line || eof=true;
      recv "$line";
      if grep -qi 'content-length' <(echo "$line") >&2; then
        CONTENT_LENGTH=$(echo "$line" | grep -Eo '[0-9]+')
      fi
      [[ -z $line ]] && break ;
    done;
    recv "$NEWFILE"

    eof=
    while [[ -z $eof ]]; do
      IFS='' read -t 0.5 -r line || eof=true;
      [[ ! $o_verbose -lt 2 ]] && recv "$line"
      o_verbose=0 send "$line" >> $NEWFILE;
    done;
    [[ -n $line ]] && send_ "$line" >> $NEWFILE
    read -r NEW_CONTENT_LENGTH < <(stat -c'%s' "$NEWFILE")

    recv "CONTENT_LENGTH  $CONTENT_LENGTH"
    recv "RECIEVED CONTENT_LENGTH $(( NEW_CONTENT_LENGTH - CONTENT_LENGTH_OFFSET))"
    
    if [[ $CONTENT_LENGTH -eq $(( NEW_CONTENT_LENGTH - CONTENT_LENGTH_OFFSET)) ]]; then
      o_verbose=1 recv "Successfully saved"
      cat "$NEWFILE" >| "$0"
      fail_with 200;
    else
      o_verbose=1 recv "Failed to save"
      fail_with 400;
    fi;
  ;;
  *)
    fail_with 405
  ;;
esac;
# vim: et:ts=2:sw=2:sts=2
