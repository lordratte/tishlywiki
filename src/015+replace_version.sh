VERSION="$(printf '%s%s' '#' "$(git describe --tags )")"
sed -i 's/^__version__$/'"$VERSION"'/g' "$1"
