srcdir="$2"
if   [ -e "$srcdir/910-sample.html" ]; then
    echo 'Using existing src/sample.html file';
else
    echo 'No existing src/sample.html file. Downloading latest from https://tiddlywiki.com/empty.html';
    curl https://tiddlywiki.com/empty.html > "$srcdir/910-sample.html"
fi;

function next_bin() {
    for cur in $(seq 1 10); do
        if [[ $(find "$srcdir/dynamic/" -name 'bin-'$cur'-*' | wc -l) -eq 0 ]]; then
            printf '%s' "$cur";
            break;
        fi
    done;
}

if [[ $(find "$srcdir/dynamic/" -name 'bin-*-handler_i586' | wc -l) -eq 0 ]]; then
    echo "Try to download handler_i586"
    curl -L https://gitlab.com/lordratte/tishlywiki-handler-rust/builds/artifacts/master/raw/target/i586-unknown-linux-musl/release/handler?job=build > "$srcdir/dynamic/bin-$(next_bin)-handler_i586"
else
    echo "Skipping existing handler_i586"
fi
