:#!/bin/bash
:: Windows Batch Section
@echo off

setlocal EnableExtensions

:uniqLoopForServerFileName
set "server=%tmp%\%~nx0~%RANDOM%.ps1"
if exist "%server%" goto :uniqLoopForServerFileName


if EXIST %server% (
    erase %server%
)

(
__PowerShellPlaceholder__
) > %server%

PowerShell -File %server% %0 %*

goto :eof
