_get_payload(){
    SF="--"">";
    PR="<!""--";
    ONE="$PR$1$SF";
    TWO="$PR$2$SF";

    if [[ $# -eq 2 ]]; then
        if [[ $1 = . && $2 != . ]]; then
            awk '{ if(match($0, "^'"$TWO"'$") || a) a=1; else print;  }'
            return;
        fi;
        if [[ $1 != .  &&  $2 != . ]]; then
            awk '{if(a) print; if(match($0, "^'"$ONE"'$")) a=1; }'
            return;
        fi;
        awk '{if(match($0, "^'"$ONE"'$")) a=1; else if(a){if(match($0, "^'"$TWO"'$")) a=0; else print} }'
    fi;
    [[ $# -eq 1 ]] && awk '{if(match($0, "^'"$ONE"'$")) a=1; else if(a){if(match($0, "^'"$PR"'[A-Za-z_0-9]+'"$SF"'$")) a=0; else print} }'
}

get_payload(){
  cat "$0" | _get_payload "$@"
}


function cmd_help() {
  cat << EOF >&2
General
  --version              Print the version of TishlyWiki
  -h,--help              Print this help text
  -v,--verbose LEVEL     Amount of output to log. Verbosity is equal to LEVEL plus number of -v flags.
                         Therefore "--verbose 1 -vv", "--verbose 2 -v" and "--verbose 3" are equivalent.
                         Level 1: Request headers. Level 2: Request bodies. Level 3: Responses.
  --exec-show            Display a table of executables embeded in this file. The 'meta' type is a run
                         configuration for the other types of executables with a matching name.
                         The 'bin' type is a compressed and encoded binary file.
                         The 'txt' type is a plain-text script.

File Conversion
Usage: $0 [options] FILE
  FILE                   The file to perform the conversion on
  -k,--convert to|from   Whether to convert the file into/from a TishlyWiki file
Example: $0 -k to empty.html

Server
Usage: $0 [options] [HOST]
  HOST is the address to bind to. Defaults to 127.0.01
  -s,--serve             Start the server
  --headless             Do not try to open in a browser
  -p,--port PORT         PORT should be a valid, open port on the host. Defaults to port in 8000-9000
  -e,--exec EXEC         If -l is not specified, EXEC is the name of an executable embeded in this file.
                         If -l is specified, EXEC is the name of a binary in your \$PATH or the path to
                         an executable.
  -l,--exec-local        See -e for more info.
  --exec-as CONFIG       Use the 'meta' configuration (see --exec-show for more info) with the name CONFIG
                         for the executable. Usefule when -l is present and EXEC isn't a recognised name.
Examples:
  $0 -e socat -s 0.0.0.0
  $0 -le socat -s 0.0.0.0
  $0 -le ./path/to/socat --exec-as socat -s 0.0.0.0
EOF
  exit;
}

params="$(getopt -o e:h,k:l,p:s,v -l convert:,exec:,exec-as:,exec-local,exec-show,help,headless,port:,serve,verbose:,version --name "$0" -- "$@")"

if [[ $? -ne 0 ]]
then
    cmd_help
fi

eval set -- "$params"
unset params

while true
do
    case $1 in
        -k|--convert)
            OPT_CONVDIR="${2-}";
            [[ $OPT_CONVDIR != to && $OPT_CONVDIR != from ]] && cmd_help;
            shift 2
            ;;
        -e|--exec)
            OPT_EXEC="${2-}";
            shift 2
            ;;
        --exec-as)
            OPT_EXEC_AS="${2-}";
            shift 2
            ;;
        -l|--exec-local)
            OPT_EXEC_LOCAL="-";
            shift
            ;;
        --exec-show)
            OPT_EXEC_SHOW="-";
            shift
            ;;
        --headless)
            OPT_HEADLESS='-';
            shift
            ;;
        -h|--help)
            cmd_help
            ;;
        -p|--port)
            OPT_PORT="${2-}";
            shift 2
            ;;
        -s|--serve)
            OPT_SERVE='-';
            shift
            ;;
        -v)
            OPT_VERBOSE="-$OPT_VERBOSE"
            shift
            ;;
        --verbose)
            OPT_TEMP_VERBOSE="${2-}";
            OPT_VERBOSE="$(for i in $(seq 1 $OPT_TEMP_VERBOSE); do echo -n '-'; done)$OPT_VERBOSE"
            shift 2
            ;;
        --version)
            head -n3 "$0" | tail -n1
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            cmd_help
            ;;
    esac
done


if [[ ! -z $OPT_EXEC && -z $OPT_EXEC_LOCAL ]]; then
  if [[ ! $OPT_EXEC == *_* ]]; then
    if [[ "$( get_payload . DATA | get_execs nometa | awk -v ex="$OPT_EXEC" '{if ($1 == ex) print;}' | wc -l )" -gt 1 ]]; then
      echo "Please specify which version you want to use ('name_version')"
      get_payload . DATA | get_execs nometa | awk -v ex="$OPT_EXEC" '{if ($1 == ex) print;}' | field_exec_exec
      exit 1
    else
      OPT_EXEC="$( get_payload . DATA | get_execs nometa | awk -v ex="$OPT_EXEC" '{if ($1 == ex) print;}' | field_exec_exec )"
    fi
  fi

  if [[ -z "$( get_payload . DATA | get_execs nometa | awk -v ex="$OPT_EXEC" '{if ($4 == ex) print;}' )" ]]; then
    echo "Could not find $OPT_EXEC embeded in this file"
    exit 1;
  fi
fi

if [[ ! -z $OPT_EXEC_SHOW ]]; then
  printf '%s\t%s\t%s\t%s\n' "Name" "Version" "Type" "Exec"
  get_payload . DATA | get_execs
  exit
fi


if [[ $OPT_CONVDIR == to ]]; then
    if [[ $(head -n 2 "$1" | tail -n1) == $(head -n 2 "$0" | tail -n1) ]]; then
        echo "Error: the file header already looks like a TishlyWiki file. Cannot convert.";
        exit 1;
    fi
    if [[ ! -w $1 ]]; then
        echo "Error: $1 is not writable.";
        exit 1;
    fi
    CODE=$(get_payload  . DATA; echo "<!""--DATA--"">");
    printf '%s\n%s' "$CODE" "$(cat "$1")" >| "$1";
    exit 0;
fi

if [[ $OPT_CONVDIR == from ]]; then
    if [[ $(head -n 2 "$1" | tail -n1) != $(head -n 2 "$0" | tail -n1) ]]; then
        echo "Error: the file header doesn't looks like a TishlyWiki file. Cannot convert.";
        exit 1;
    fi
    if [[ ! -w $1 ]]; then
        echo "Error: $1 is not writable.";
        exit 1;
    fi
    DATA="$(awk '{if (a) print; if(match($0, "^<!''--DATA--''>$")) a=1}END{if (!a) exit 1}' "$1")";
    if [[ $? -ne 0 ]]; then
        echo "Error: DATA section not found. The TishlyWiki may be corrupt."
        exit 1;
    fi
    printf '%s' "$DATA" >| "$1";
    exit 0;
fi

if [[ -z $ishandler ]]; then
    [[ -z $OPT_SERVE ]] && cmd_help;
    if [[ -z $OPT_EXEC ]]; then
      echo "No executable selected";
      cmd_help;
    fi

    if [[ -z $1 ]]; then
      host='127.0.0.1';
    else
      host="$1";
    fi;

    EXETAG="$OPT_EXEC"

    if [[ -z "$OPT_EXEC_LOCAL" ]]; then
      EXETYPE="$(get_payload . DATA | get_execs nometa | awk -v ex="$OPT_EXEC" '{if ($4 == ex) print}' | head -n1 | field_exec_type )"
      EXE="$(mktemp --suffix="_$EXETAG")"
      [[ "${#OPT_VERBOSE}" -gt 0 ]] && echo "Writing the executable to $EXE"
      if [[ $EXETYPE == 'bin' ]]; then
        get_payload "BIN$EXETAG" | grep -vE '^<' | base64 -d | lzma -d > $EXE
      elif [[ $EXETYPE == 'txt' ]]; then
        get_payload "TXT$EXETAG" | tail -n+2 | head -n-1 > $EXE
      fi
      chmod +x $EXE
      TRAPEXE="rm $EXE 2> /dev/null || true"
    else
      EXE="$EXETAG"
    fi

    [[ -z $OPT_EXEC_AS ]] && OPT_EXEC_AS="$(echo "$EXETAG" | grep -Eo '^[^_]+' )"
    RUN="$(get_payload "META$OPT_EXEC_AS" | grep -vE '^<' | head -n1 )"
    if [[ -z $RUN ]]; then
      echo "Warning: meta '$OPT_EXEC_AS' not found. Using default run configuration." >&2
      RUN='o_host="$host" o_port="$port" o_verbose="${#OPT_VERBOSE}" o_fname="$0" ishandler=true $EXE'
    fi
    
    if [[ -z $OPT_PORT ]]; then
        port=$(comm -23 <(seq 8000 9000 | sort) <(ss -Htan | awk '{print $4}' | cut -d':' -f2 | sort -u) | sort | head -n 1); # Get next open port
    else
        port="$OPT_PORT";
    fi
    
    echo "Try to listen on http://$host:$port";
    trap 'kill $(jobs -p) 2>/dev/null; '"$TRAPEXE" EXIT INT TERM
    [[ -z $OPT_HEADLESS ]] && (xdg-open "http://$host:$port" || echo "Couldn't open browser");
    eval "$RUN";
    exit 0;
fi
# vim: et:ts=2:sw=2:sts=2
