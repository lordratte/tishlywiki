for bin in $( find "$2/dynamic" -name 'bin-*' | grep -Ev '\.meta$' | sort); do
    file=$(basename $bin);
    sequence="$(echo "$file" | grep -Eo '\-[01-9]+-' | grep -Eo '[0-9]+' )"
    tag="$(echo "$file" | grep -Eo '\-[a-z_0-9]+$' | grep -Eo '[^-]+' )"

    if [[ -z $tag || $tag != *_* ]]; then
        echo "Invalid tag for $file"
        exit 1
    fi
    if [[ -z $sequence ]]; then
        echo "Invalid sequence for $file"
        exit 1
    fi
    
    echo '<!--BIN'"$tag"'-->' >> "$1"
    echo '<script type="text/vnd.tishlywiki.bin.'"$tag"'">' >> "$1"
    cat "$bin" | lzma -9 | base64 -w300  >> "$1"
    echo '</script>' >> "$1"
    echo '<!--END'"$tag"'-->' >> "$1"

done;

for txt in $( find "$2/dynamic" -name 'txt-*' | grep -Ev '\.meta$' | sort); do
    file=$(basename $txt);
    sequence="$(echo "$file" | grep -Eo '\-[01-9]+-' | grep -Eo '[0-9]+' )"
    tag="$(echo "$file" | grep -Eo '\-[a-z_0-9]+$' | grep -Eo '[^-]+' )"

    if [[ -z $tag || $tag != *_* ]]; then
        echo "Invalid tag for $file"
        exit 1
    fi
    if [[ -z $sequence ]]; then
        echo "Invalid sequence for $file"
        exit 1
    fi
    
    echo '<!--TXT'"$tag"'-->' >> "$1"
    echo '<script type="text/vnd.tishlywiki.txt.'"$tag"'">' >> "$1"
    cat "$txt" | sed 's/&/\&amp;/g; s/</\&lt;/g; s/>/\&gt;/g;'  >> "$1"
    echo '</script>' >> "$1"
    echo '<!--END'"$tag"'-->' >> "$1"

done;

for meta in $( find "$2/dynamic" -name 'bin-*' | grep -E '\.meta$' | sort); do
    file=$(basename $meta);
    sequence="$(echo "$file" | grep -Eo '\-[01-9]+-' | grep -Eo '[0-9]+' )"
    tag="$(echo "$file" | grep -Eo '\-[a-z0-9]+.meta$' | grep -Eo '\-[a-z0-9]+' | grep -Eo '[^-]+' )"

    if [[ -z $tag ]]; then
        echo "Invalid tag for $file"
        exit 1
    fi
    if [[ -z $sequence ]]; then
        echo "Invalid sequence for $file"
        exit 1
    fi
    
    echo '<!--META'"$tag"'-->' >> "$1"
    echo '<script type="text/vnd.tishlywiki.meta.'"$tag"'">' >> "$1"
        cat "$meta" >> "$1"
    echo '</script>' >> "$1"
    echo '<!--END'"$tag"'-->' >> "$1"

done;
