(function(){
    "use strict";
    var self = document.currentScript;
    var text = [].filter.apply(self.parentElement.childNodes, [(o)=>o.nodeType===3 && o.data.match(/^.{0,4}(goto +)?#!\/bin\/bash/)]).pop();
    var shebang = '#!/bin/bash'
    if (text) {
        shebang = text.textContent.split("\n").shift();
        text.remove();
    }
    function aff(sec) {return '<!'+'--'+sec+'--'+'>';}

    var get_file = new Promise(function(resolve, reject) {
        try {
            window.addEventListener('load', (event) => {
                resolve([
                    document.querySelector("[type='text/vnd.tishlywiki.1']").innerText,
                    self.innerText,
                    [].filter.apply(document.querySelectorAll(("script[type^='text/vnd.tishlywiki.']")), [e=>-1<e.type.search(/^text\/vnd\.tishlywiki\.(bin|meta|txt)/)]).map(e=>{
                        return {
                            'typeprop': e.type,
                            'body': e.innerText,
                            'tag': e.type.match(/[^.]+$/).pop(),
                            'type': e.type.match(/(bin|txt|meta)\.[^.]+$/).pop()
                      }
                    }).map(e=>[
                             aff(e.type.toUpperCase()+e.tag)+'\n',
                             '<script type="{typeprop}">'.replace('{typeprop}', e.typeprop),
                             e.body , '</script'+'>\n',
                             aff('END'+e.tag)
                             ].join('')).join('\n')
                ]);
            });

        } catch (e) {
            reject(e);
        }
    });

    var get_twobj = new Promise(function(resolve, reject) {
        try {
            function gettw() {
                if (window.$tw && window.$tw.wiki && $tw.wiki.addTiddler) {
                    resolve(window.$tw);
                } else {
                    setTimeout(gettw, 1000);
                }
            }
            gettw();
        } catch (e) {
            reject(e);
        }
    });

    Promise.all([get_file, get_twobj]).then(function([scripts, tw]) {

        tw.wiki.addTiddler({
            'title': '$:/temp/tishlywiki',
            'type': 'text/plain',
            'text':  shebang + '\n' +
                             '#<script type="text/vnd.tishlywiki.1">' +
                             scripts[0] +
                             '</script'+'>\n' +
                             aff('BOOT') + '\n' +
                             '<script'+'>' +
                             scripts[1] +
                             '</script'+'>' + '\n' +
                             scripts[2] + '\n' +
                             aff('DATA')
        });
        var o = $tw.wiki.getTiddlerText('$:/core/save/all');
        if (!o.match(/tishlywiki/)) {
          var n = o.replace('\n{{$:/core/templates/tiddlywiki5.html}}', '\n{{$:/temp/tishlywiki}}\n{{$:/core/templates/tiddlywiki5.html}}');
          if(!n) throw Error('TishlyWiki saver couldn\'t install normally');
          $tw.wiki.setText('$:/core/save/all', null, null, n );
        }
    }).catch(function(e) {
        console.error(e);
    });

})();
/* vim: et:ts=4:sw=4:sts=4
*/
