recv() { [[ $o_verbose -gt 0 ]] && echo "< $@" >&2; }
send_() { [[ $o_verbose -gt 2 ]] && echo "> $@" >&2;
         printf '%s' "$*"; }
send() { [[ $o_verbose -gt 2 ]] && echo "> $@" >&2;
         printf '%s\n' "$*"; }

send_response() {
  local code=$1
  send "HTTP/1.0 $1 ${HTTP_RESPONSE[$1]}"
  for i in "${RESPONSE_HEADERS[@]}"; do
    send "$i"
  done
  send
  while IFS='' read -r line; do
    send "$line"
  done
  [[ -n $line ]] && send_ "$line";
}

fail_with() {
  send_response "$1" <<< "$1 ${HTTP_RESPONSE[$1]}"
  exit 1
}

send_response_ok_exit() { send_response 200; exit 0; }

DATE=$(date +"%a, %d %b %Y %H:%M:%S %Z")
declare -a RESPONSE_HEADERS=(
  "Date: $DATE"
  "Expires: $DATE"
  "Server: $(basename "$0")"
)
declare -a HTTP_RESPONSE=(
 [200]="OK"
 [400]="Bad Request"
 [403]="Forbidden"
 [404]="Not Found"
 [405]="Method Not Allowed"
 [500]="Internal Server Error"
)

add_response_header() {
  RESPONSE_HEADERS+=("$1: $2")
}

serve_file() {
  local file="$1"

  add_response_header "Content-Type" "text/html";

  CONTENT_LENGTH=$(printf '%s' "$file" | wc -c );
  add_response_header "Content-Length" "$CONTENT_LENGTH";

  printf '%s' "$file" | send_response_ok_exit
}
# vim: et:ts=2:sw=2:sts=2
