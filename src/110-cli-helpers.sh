if [[ -z $ishandler ]]; then
    get_execs() {
        awk '/^<!--(BIN|META|TXT)[a-z_0-9]+-->$/ {print}' | while read -r line; do
            typ="$( echo "$line" | grep -Eo '[A-Z]+' | awk '{print tolower($0)}')"
            [[ $1 == 'nometa' && $typ == 'meta' ]] && continue;

            line="$(echo "$line"| grep -Eo '[a-z_0-9]+' )"
            if [[ -z $(echo "$line" | grep '_' ) ]] ; then
                line="${line}_?"
            fi
            printf '%s\t%s\t%s\t%s\n' "$( echo "$line" | grep -Eo '^[^_]+' )" "$( echo "$line" | grep -Eo '[^_]+$' )" "$typ" "$line"
        done
    }

    field_exec_name() { cut -f1 | sort | uniq; }
    field_exec_version() { cut -f2 | sort | uniq; }
    field_exec_type() { cut -f3 | sort | uniq; }
    field_exec_exec() { cut -f4 | sort | uniq; }

fi
