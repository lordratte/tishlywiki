Note: the example TishlyWiki file will be `build.html`

# Windows
Please first ensure that you have one of the [Windows methods set up](./Windows.md)

## Windows Subsystem for Linux (Recommended)
Open a bash terminal on Windows in the directory of the TishlyWiki file and refer to the linux section.

## Batch and Powershell (Emergency use-case)
Either double-click on `build.html.bat` or open a new cmd in the directory of the TishlyWiki file and run `.\build.html.bat` at the prompt.

If you would like to specify a custom port or host use `port=...` or `host=...` parameters respectively e.g. `.\build.html.bat port=8080 host=+` (which will make the server accessible from anywhere on your network on port 8080). You may need admin permissions to do this depending on the port and host you use.

If you would like to use more advanced features, use the WSL method.

# Linux
If the filesystem allows executable files, make sure the file is executable `chmod +x build.html`. You can then run it directly e.g. `./build.html ...`

Otherwise it's fine to invoke it as an argument to bash e.g. `bash build.html`

To see what this TishlyWiki can do, run with the `--help` flag.

## Server Mode
Server mode is invoked by adding the `-s` flag.

Since TishlyWiki's goal is to be agnostic to OS and architecture, you must specify what you want use for the server executable.

To see what executables are available, you can run `./build.html --exec-show`

The output will look something like this:
```
Name	Version	Type	Exec
handler	i586	bin	handler_i586
socat   x64     bin     socat_x64
socat   arm7    bin     socat_arm7
python	python	txt	python_python
socat	?	meta	socat_?
ncat	?	meta	ncat_?

```

The Exec column is what you will specify with the `-e` flag to specify which executable to use. If there is only one instance of an executable name in the Name column (exluding types marked "meta") you can just specify that e.g. `-e handler`.

Otherwise the full Executable should be specifed e.g. `-e socat_x64`

### Types of executables
#### bin
This is an executable binary that has been compressed with lzma. base64 encoded and embeded in the file. This must be used on the right OS if it's a dynamically linked executable and the CPU must support the arctitecture.
#### meta
This is a specification for how a binary executable should run. For instance, socat is run like this:
`socat TCP4-LISTEN:"$port",fork,bind="$host" SYSTEM:'bash "$o_fname"'` where $o_fname is the name of the script.
And ncat is run like this:
`ncat -c 'bash "$o_fname"' -kl "$host" "$port"`

Whenever TishlyWiki uses one of those executables to host its server it needs to know that that's the way that executable is used. Whenever a "bin" type is specified by the `-e` flag, it looks for a corrospondingly named "meta" type which specifies how it should be run.

Sometimes you may want a "meta" type available even though there is no equivalent binary embeded. If you run `./build.html -le ncat` it looks for ncat locally rather than trying to extract a payload. This is helpful if none of the embeded binaries support your current architecture.
#### txt
This is an embeded, plain-text executable. It is executed by the programme specified by Version (which needs to be present on the system).

### Modes and Defaults
By default, TishlyWiki will try to open the locally configured browser to the right url when serving by using `xdg-open`. If the system doesn't support this or this bugs out, you can skip this step by running with the `--headless` flag. Sometimes, when it opens the page the browser will tell you the page could not be found. This could be becase the page opened before the server was up or because it was redirected to https from http. Check the URL and refresh.

For some vague semblance of security, the sever is hosted on localhost by default so that other computers on your network cannot access the URL. If you like to do dangerous things, you can specify another ip to listen on as the first positional argument like this `./build.html 0.0.0.0 -s -e handler` (in this case all interfaces are being listened on).
