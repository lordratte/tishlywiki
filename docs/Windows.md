# Windows Subsystem for Linux (Recommended)
Some time ago Windows added support to run linux programmes using a custom subsystem. You can install it on Windows with [these instructions](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

Using TishlyWiki in WSL works surprisingly well. Just open bash to the location of your TishlyWiki file and use it as you normally would have on Unix.

Note: So far, only Ubuntu was tested.

# Batch and Powershell (Emergency use-case)
Windows needs some extra steps for TishlyWiki to work natively using batch and powershell. This is an emergency feature when you can't use WSL for whatever reason.
 
Note for those using this on both Unix and Windows: Following these steps will possibly stop the file from running on Unix until you revert it (if bash isn't your default shell). If it does work, your terminal may get spammed with messages saying that the "goto" command can't be found. This looks ugly, but at least it works.

## Step 1
Change the TishlyWiki file to give it a `.bat` extension.

## Step 2
Open the TishlyWiki in notepad and insert the phrase `goto ` as the first 5 characters of the file (instead of just `#!/bin/bash`).

![](./img/Windows_1.png)

## Step 3

Instead of saving the file, choose Save As. Then select ANSI as the encoding. If you don't, notepad will insert a [BOM](https://en.wikipedia.org/wiki/Byte_order_mark) at the start of the file which will explode when you try to run the file (or something like that).

![](./img/Windows_2.png)

## Step 4

After clicking "Save", politely ignore the warning by clicking "Ok".

![](./img/Windows_3.png)

## Reverting
Go through the steps in order and undo the changes you made i.e. remove the `.bat` extension; remove the leading `goto `.
