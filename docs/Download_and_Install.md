# Method 1: Pre-made file

1. Download the latest stable copy of a TishlyWiki [here](https://gitlab.com/lordratte/tishlywiki/-/jobs/artifacts/master/raw/out/build.html?job=build). Note: this comes with an unmodified `$:/core/save/all` tiddler. The first time you open it as an html file, it will modify the this tiddler.

2. Open a terminal in the downloaded directory

3. Run `chmod +x buld.html` to make it executable.

# Method 2: Command Line Method

If you already have access to a TishlyWiki file from version 0.0.5 or later, you can use command line arguments to create, uncreate and upgrade TishlyWiki files.

For example, if your TishlyWiki file is called `build.html` and your ordinary TiddlyWiki is called `empty.html`:

* Run `build.html -k to empty.html` to turn `empty.html` into a TishlyWiki file.

* Run `build.html -k from empty.html` to turn it back into a normal TiddlyWiki.

* Run `build.html -k from build.html` if you want a TishlyWiki file to turn *itself* back into a TiddlyWiki file. Keep in mind that it will be "stranded" as a normal html file until you turn it into a TiddlyWiki file again using another method. So it's advisable to make the change on a copy.

* As a corollary, this works on most stand-alone HTML files e.g. `build -k to index.html`. `index.html` can still be viewed as normal and can also be run as a stand-alone server but it cannot save to itself unless it implements the protocol itself.

# Method 3: Conversion tiddlers

**Warning 1**: if you have a different or custom version of the `$:/core/save/all` tiddler, use Method 3 instead.

**Warning 2**: don't do this to an existing TishlyWiki that you are accessing from the executable mode. Rather open the file directly as if it were a normal TiddlyWiki file.

If you want to convert an existing TiddlyWiki into a TishlyWiki, install the convenience tiddlers [here](https://gitlab.com/lordratte/tishlywiki/-/jobs/artifacts/master/raw/out/tishlywiki.json?job=build)

What does this contain?

* A modified version of `$:/core/save/all`

* A copy of the main TishlyWiki code, named `:/temp/tishlywiki`

Once you have imported these, rename `:/temp/tishlywiki` to `$:/temp/tishlywiki`. This step is needed because temporary tiddlers cannot be imported.

When you save again, the file will now be a TishlyWiki file.

# Method 4: Manual method (special cases)

**Warning**: don't do this to an existing TishlyWiki that you are accessing from the server mode. Rather open the file directly as if it were a normal TiddlyWiki file and use one of the regular savers.

If you want to modify your `$:/core/save/all` tiddler manually, you can simply import the stand-alone `$:/temp/tishlywiki` tiddler, [here](https://gitlab.com/lordratte/tishlywiki/-/jobs/artifacts/master/raw/out/temp_tishlywiki.tid?job=build) into your wiki and reference it yourself.

As with Method 3, rename `:/temp/tishlywiki` to `$:/temp/tishlywiki`. This is needed because temporary tiddlers cannot be imported.

The TishlyWiki tiddler now contains everything that needs to be prepended to a TiddlyWiki to make it executable.

**To reference it manually**:

1. Modify your `$:/core/save/all` tiddler. You can find it by searching for it under Shadows (in the advanced search).

2. Find the line that reads `{{$:/core/templates/tiddlywiki5.html}}`. Insert a line before it containing `{{$:/temp/tishlywiki}}`

3. Save the wiki and reload
