**Dependencies**

* Python, to JSON-escape the code.

* Bash

* GNU cURL

**Process**

Run `./build`

Output will be in the `out/` directory.

You can use an existing TiddlyWiki file in the compile process by replacing `src/910-sample.html` with the TiddlyWiki file you want to use.

By default, the only binary file that is embeded in the code is a custom TCP handler written in Rust for this purpose (https://gitlab.com/lordratte/tishlywiki-handler-rust).

If you would like to include your own server executables (like an embeded socat binary), copy it into the `src/dynamic` folder using the following naming convension:

bin-*SEQUENCE*-*NAME*_*PLATFORM* e.g. bin-1-socat_x86

The PLATFORM can be anything and is there to distinguish between multiple binaries of the same name.

If your binary needs to be run in a special way, add a Meta payload for it in the following format:

bin-*SEQUENCE*-*NAME*.meta

See this [socat example](./src/dynamic/bin-1-socat.meta)